---
name: 'Hackathon website template'
link: https://github.com/Alex-CD/hackathon-template-jekyll
index: 2
---

A base for hackathon sites. Forked from the site I built as part of the [Royal Hackaway](https://royalhackaway.com/) team in 2018.
