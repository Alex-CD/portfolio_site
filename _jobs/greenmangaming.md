---
name: 'Green Man Gaming'
role: 'Backend Software Engineer'
link: https://corporate.greenmangaming.com/careers/
period: 'June 2020 - Oct 2021'
index: 2
---

Backend software engineer on the Green Team :)
Built and maintained a range of store and business intelligence systems with Python and GoLang.
