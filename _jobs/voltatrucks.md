---
name: 'Volta Trucks'
role: 'Backend Engineer'
link: https://voltatrucks.com/
period: 'October 2022 - May 2023'
index: 1
---

Built a vehicle diagnostics backend from the ground up on a .NET 7 stack.
